# Car Shop Application
[Barz Sebastian Mihai]

## Introducere
Am ales ca pentru acest proiect construiesc o aplicatie pentru o companie sau o persoana care vrea sa vanda masini. Aceasta va avea doua tipuri de utilizatori, useri si admini. Userii au acces la lista de masini din magazin, toate detaliile acestora si le pot achizitiona. Adminii sunt cei ce gestioneaza masinile. Acestia adauga masinile in baza de date, pot modifica datele sau sterge o masina. Aplicatia foloseste serviciile REST, este scrisa in jimbajul JAVA, foloseste clientul HTTP Axios si totul este facut utilizand mediul de programare IntelliJ IDEA. Pe de alta  parte, front-end-ul aplicatiei este creat in WebStorm, un IDE creat tot de JetBrains. Am ales aceasta varianta deoarece nu am mai lucrat deloc in Android, dar aplicatii web am facut mai multe.

## Cerinte
Aplicatia foloseste framework-ul Spring Boot, respecta principiile SOLID si foloseste serviciile REST. Pentru baza de date am ales PostgreSQL si am creat-o folosind pgAdmin. Aplicatia finala urmeaza sa fie una Web, avand o pagina de Log In, unde utilizatorii normali sa se poata loga folosind un user si o parola. Apoi acestia pot vizualiza o lista cu anunturi, fiecare anunt avand un buton dedicat pentru achizitionarea masinii. Lista poate fi filtrata dupa mai multe preferinte(model, pret, etc.). Adminul este in principiu tot un utilizator, se va loga cu credentialele lui, dar este singurul care are acces si poate modifica lista de masini din magazin. Acesta poate adauga, modifica sau sterge masini dupa bunul plac. Cand adminul va introduce o masina noua in lista, userii vor primi o notificare. Acest lucru va trebui implementat pe viitor, utilizand paternul Observer. Notificarea este afisata in consola IntelliJ, dar ca o imbunatatire ulterioara, acesta poate fi afisat direct in browser. Pentru partea finala a proiectului, mai exact front-end-ul aplicatiei, am ales sa construiesc  o aplicatie web, deoarece experienta mea  in acest domeniu este mult mai mare fata de android, unde nu am lucrat deloc.

## Implementare
Partea de back-end a aplicatiei este pregatita, am reusit sa o implementez folosind 5 pachete care ma ajuta sa respect cerintele functionale ale acesteia: controllers, dtos, entities, repositories si servicies. Acestea sunt indeajuns pentru a mentine aplicatia intr-o arhitectura pe mai multe layere. Endpoint-ul testat in Postman este cel pentru masini, avand 4 operatii functionale: GET, POST, PUT si DELETE. Acesta se noteaza cu "/car", unde se adauga, modifica si vizualizeaza masini si functioneaza perfect, masina este adaugata in baza de date si toate operatiile se pot efectua corect. Celelalte end-point-uri functioneaza asemanator, fiind posibile toate operatiile specifice REST.
Pentru partea de front-end, am ales sa construiesc, pornind de la un exemplu de la alta materie, o aplicatie in web, care sa ma ajute sa implementez toate functionalitatile din back-end. A fost un task destul de complex, dar pe care am ajuns sa il duc la bun sfarsit. Pentru inceput am creat directoare pentru fiecare entitate din proiectul initial. Acestea contin cate un api, niste componente (tabele, formuri, etc) si un container pentru fiecare. In api se implementeaza functiile necesare si se defineste endpoint-ul la care se leaga. Componentele sunt formate fiecare in clasa proprie si acestea sunt conectate impreuna in clasa container, unde se afla si functia de render si se mai implementeaza diferite functii. Partea de login contine un formular in care trebuie introduse username-ul si parola, iar pe baza acestor credentiale, cat si pe baza rolului pe care utilizatorul il detine, acesta este redirecttionat care homepage, care arata diferit pentru fiecare rol in parte. Am reusit sa fac acest lucru prin crearea  unor constante care sa ma ajute sa imi  dau seama daca este vorba de admin sau de un utilizator normal. Mai apoi am construit niste rute pentru fiecare end-point si am decis pentru ce roluri pot si vizibile si accesabile acele rute. Cu ajutorul acestor rute protejate am reusit sa fac redirect daca un utilizator incearca sa intre  pe un link care nu ii este destinat lui.    

## Endpoints

    /car: aici au loc operatiile CRUD pentru adaugare si de afisare a tuturor masinilor.
    
    /car/{id}: cu ajutorul id-ului se poate sterge, modifica sau afisa o anumita masina.
    
    /login: este endpointul unde se face logarea userului dupa credentiale: username si parola
    
    /person: este asemanator cu endpointul de la masini. Pe /person se  face adaugarea unui user nou si afisarea tuturor userilor. Aici se pot adauga atat admini, cat si useri, deoarece functia aceasta este aleasa cand se face un cont nou.
    
    /person/{id}: cu ajutorul id-ului unei persoane se pot modifica datele acestuia, se poate sterge contul sau se pot afisa informatiile aferente.
    
    /person/pdf: se creaza un raport cu toate masinile in format pdf.
        
    /person/txt: se creaza un raport cu toate masinile in format txt.
        
    /person/html: se creaza un raport cu toate masinile in format html.

## Diagrama Deployment

![deployment_diagram](https://i.ibb.co/1KTP3h0/deploy.png)

## Diagrama Observer Pattern

![Observer_pattern_diagram](https://i.ibb.co/VpjyVPd/observer.png)

## Abstract Factory pattern

Am ales sa folosesc Abstract Factory pattern deoarece am considerat ca ar fi util ca aplicatia mea sa aiba o optiune de scriere a unui raport. Acest raport consta in listarea tuturor masinilor, impreuna cu detaliile aferente lor. Este o metoda buna de a vizualiza toate masinile din magazin si de a si salva lista acestora dintr-un anumitr moment. Ca paternul sa aiba o utilitate, am ales sa implementez mai multe tipuri de rapoarte. Acesta poate sa fie pdf, txt sau html, in functie de alegerea utilizatorului. Aceste optiuni au fecare cate un endpoint, iar aceste endpoint-uri se vor putea accesa printr-o simpla apasare a unui buton. Vor exista 3 butoane, unul pentru fiecare optiune. Fisierul va fi creat in folderul sursa al  proiectului, mai exact in folderul src. Acestea au un nume intuitiv "masini",  iar extensia va fi aleasa in functie de butonul apasat de utilizator.

## Front-End

Dupa ce am reusit sa implementez toate functionalitatile pe care le-am propus, urmatorul pas este de a face designul pentru interfata grafica prin care utilizatorul interactioneaza cu sistemul nostru. Pentru partea de front-end, am ales sa construiesc, pornind de la un exemplu de la alta materie, o aplicatie in web, care sa ma ajute sa implementez toate functionalitatile din back-end. A fost un task destul de complex, dar pe care am ajuns sa il duc la bun sfarsit. Pentru inceput am creat directoare pentru fiecare entitate din proiectul initial. Acestea contin cate un api, niste componente (tabele, formuri, etc) si un container pentru fiecare. In api se implementeaza functiile necesare si se defineste endpoint-ul la care se leaga. Componentele sunt formate fiecare in clasa proprie si acestea sunt conectate impreuna in clasa container, unde se afla si functia de render si se mai implementeaza diferite functii. Partea de login contine un formular in care trebuie introduse username-ul si parola, iar pe baza acestor credentiale, cat si pe baza rolului pe care utilizatorul il detine, acesta este redirecttionat care homepage, care arata diferit pentru fiecare rol in parte. Am reusit sa fac acest lucru prin crearea  unor constante care sa ma ajute sa imi  dau seama daca este vorba de admin sau de un utilizator normal. Mai apoi am construit niste rute pentru fiecare end-point si am decis pentru ce roluri pot si vizibile si accesabile acele rute. Cu ajutorul acestor rute protejate am reusit sa fac redirect daca un utilizator incearca sa intre  pe un link care nu ii este destinat lui.

## Testare
Am implementat 3 teste, unul pentru o inserare obisnuita si inca 2 care testeaza clasele care valideaza datele de intrare. Utilizatorii inserati in baza de date nu pot avea campul de adresa lasat null sau sa fie minori. Acestea testeaza controllerele pentru persoana si validatorii pentru varsta. Testele functioneaza corect, iar pe viitor am sa dezvolt testare si pentru clasere din pachetul services. 
Acestea ne ofera siguranta ca applicatia functioneaza corect si toate modulele sunt legate corect.

## Imbunatatiri pe viitor
Aplicatia va fi in final una web, partea de back-end fiind aproape gata de deploy. Va trebui sa construiesc un front-end care contine o pagina de log in, iar apoi pe baza credentialelor vei fi redirectionat ori catre pagina adminului, unde se poate modifica lista de masini, ori spre pagina de user normal, unde se pot vizualiza si achizitiona. O imbunatatire ar fi designul, care  deocamdata este destul de rudimental. Pentru o aplicatie destinata clientilor, aceasta ar avea nevoie  de mult mai multa munca la partea de design. Imbunatatirile pe partea de servicii ar fi multe  la numar, acest domeniu de  e-commerce dezvoltandu-se din ce in ce mai mult in ultima perioada. Se pot lega multe alte servicii, cum ar fi plata cu cardul sau distribuirea anunturilor pe alte pagini de socializare sau aplicatii de mesaje. 
