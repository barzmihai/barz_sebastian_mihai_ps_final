import {HOST} from '../../commons/hosts';
import {performRequest} from "../../commons/api/rest-client";


const endpoint = {
    person: '/person'
};

function getPersons(callback) {
    let request = new Request(HOST.local_api + endpoint.person, {
        method: 'GET',
    });
    console.log(request.url);
    performRequest(request, callback);
}

function getPersonById(params, callback){
    let request = new Request(HOST.local_api + endpoint.person + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    performRequest(request, callback);
}

function postPerson(user, callback){
    let request = new Request(HOST.local_api + endpoint.person , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    performRequest(request, callback);
}

function updatePerson(oldName, user, callback){
    let request = new Request(HOST.local_api + endpoint.person + "/"+oldName, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    performRequest(request, callback);
}

function deletePerson(person, callback){
    let request = new Request(HOST.local_api + endpoint.person + "/" + person.name, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log("URL: " + request.url);

    performRequest(request, callback);
}

export {
    getPersons,
    getPersonById,
    postPerson,
    deletePerson,
    updatePerson
};
