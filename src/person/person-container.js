import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PersonForm from "./components/person-form";
import DeleteForm from "./components/delete-form";
import UpdateForm from "./components/update-form";

import * as API_USERS from "./api/person-api"
import PersonTable from "./components/person-table";




class PersonContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.deleteForm = this.deleteForm.bind(this);
        this.updateForm = this.updateForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.state = {
            selected: false,
            selectedDelete: false,
            selectedUpdate: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        return API_USERS.getPersons((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPersons();
    }

    deleteForm() {
        this.setState({selectedDelete: !this.state.selectedDelete});
    }


    reloadDelete() {
        this.setState({
            isLoaded: false
        });
        this.deleteForm();
        this.fetchPersons();
    }

    updateForm() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }


    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.updateForm();
        this.fetchPersons();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Person Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Person </Button>
                            <Button color="primary" onClick={this.deleteForm}>Delete Person </Button>
                            <Button color="primary" onClick={this.updateForm}>Update Person </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PersonTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Person: </ModalHeader>
                    <ModalBody>
                        <PersonForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedDelete} toggle={this.deleteForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.deleteForm}> Delete Person: </ModalHeader>
                    <ModalBody>
                        <DeleteForm reloadHandler={this.reloadDelete}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.updateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.updateForm}> Update Person: </ModalHeader>
                    <ModalBody>
                        <UpdateForm reloadHandler={this.reloadUpdate}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default PersonContainer;
