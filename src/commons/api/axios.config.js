import axios from "axios";
import {HOST} from "../hosts";

export const httpClient = axios.create({
    baseURL: HOST.local_api
});