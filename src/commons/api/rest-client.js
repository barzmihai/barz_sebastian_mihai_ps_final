import {httpClient} from "./axios.config";

export const performRequest = (request, callback) => {
    fetch(request)
        .then(
            function (response) {
                if (response.ok) {
                    response.json().then(json => callback(json, response.status, null));
                } else {
                    response.json().then(err => callback(null, response.status, err));
                }
            })
        .catch(function (err) {
            //catch any other unexpected error, and set custom code for error = 1
            callback(null, 1, err)
        });
}

export const get = (url, params) => {
    return httpClient.get(url, {params});
}

export const post = (url, data) => {
    return httpClient.post(url, data);
}

export const put = (url, data) => {
    return httpClient.put(url, data);
}

export const del = (url, params) => {
    return httpClient.delete(url, {params});
}