import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {getUserRole, isAuth} from "./utils";

export const ProtectedRoute = ({component: Component, forRoles, ...rest}) => {
    return <Route
        {...rest}
        render={() => {
            if (!isAuth()) {
                return <Redirect to={'/login'}/>
            }
            if (forRoles !== undefined) {
                if (!forRoles.includes(getUserRole())) {
                    return <Redirect to={`/${getUserRole().toLowerCase()}`}/>
                }
            }
            return <Component {...rest}/>
        }}
    />
}
