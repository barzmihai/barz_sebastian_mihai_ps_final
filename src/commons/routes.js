import Home from "../home/home";
import PersonContainer from "../person/person-container";
import DoctorContainer from "../doctor/doctor-container";
import {DoctorLanding} from "../pages/doctor/doctor.landing";
import {CaregiverLanding} from "../pages/caregiver/caregiver.landing";
import {PatientLanding} from "../pages/patient/patient.landing";
import {UserRole} from "./constants";

export const routes = [
    {
        exact: true,
        path: '/person',
        component: PersonContainer,
        forRoles: [UserRole.DOCTOR]
    },
    {
        exact: true,
        path: '/doctors',
        component: DoctorContainer,
        forRoles: [UserRole.DOCTOR]
    },
    {
        exact: true,
        path: '/doctor',
        component: DoctorLanding,
        forRoles: [UserRole.DOCTOR]
    },{
        exact: true,
        path: '/caregiver',
        component: CaregiverLanding,
        forRoles: [UserRole.CAREGIVER]
    },{
        exact: true,
        path: '/patient',
        component: PatientLanding,
        forRoles: [UserRole.PATIENT]
    },
]