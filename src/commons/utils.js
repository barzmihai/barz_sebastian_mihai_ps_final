import {ROLE_KEY} from "./constants";

export const getUserRole = () => localStorage.getItem(ROLE_KEY)

export const isAuth = () => {
    const userRole = localStorage.getItem(ROLE_KEY);
    return userRole !== null && userRole !== undefined;
}

export const isFieldValid = value => value !== undefined && value !== null && value.trim() !== '';