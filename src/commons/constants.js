export const ROLE_KEY = 'role';

export const UserRole = {
    DOCTOR: 'DOCTOR',
    CAREGIVER: 'CAREGIVER',
    PATIENT: 'PATIENT'
}

export const ROLES_WITH_PERMISSION = [UserRole.DOCTOR];