import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Alert, Button, Container, Form, FormGroup, Input, Jumbotron, Label} from 'reactstrap';
import {isFieldValid} from "../commons/utils";
import {post} from "../commons/api/rest-client";
import {ROLE_KEY} from "../commons/constants";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    backgroundImage: `url(${BackgroundImg})`,
    objectFit: 'cover'
};

class Login extends React.Component {

    state = {
        validEmail: true,
        validPassword: true,
        loginError: ''
    }

    handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();
        const email = event.target.email.value;
        const password = event.target.password.value;
        let fieldsValid = true;
        if (!isFieldValid(email)) {
            this.setState({validEmail: false});
            fieldsValid = false;
        }
        if (!isFieldValid(password)) {
            this.setState({validPassword: false});
            fieldsValid = false;
        }
        if (fieldsValid) {
            post('/login', {username: email, password})
                .then(({data}) => {
                    if (data) {
                        localStorage.setItem(ROLE_KEY, data);
                        this.props.history.push(`/${data.toLowerCase()}`);
                    }
                })
                .catch(() => this.setState({loginError: 'Invalid credentials'}))
        }
    }

    render() {
        const {validEmail, validPassword, loginError} = this.state;
        return (
            <div className={"pages-container"}>
                <Jumbotron fluid style={backgroundStyle} className={"mb-0 h-100"}>
                    <Container fluid style={{maxWidth: '600px'}}>
                        <div
                            className='bg-white rounded p-4 d-flex flex-column justify-content-center align-items-center shadow-sm'>
                            {loginError && <Alert color="danger" style={{width: '50%', maxWidth: '300px'}}>
                                {loginError}
                            </Alert>}
                            <Form onSubmit={this.handleSubmit} style={{width: '50%', maxWidth: '300px'}}>
                                <FormGroup>
                                    <Label for="username">Email</Label>
                                    <Input
                                        type="email"
                                        name="email"
                                        id="username"
                                        placeholder="Username"
                                        invalid={!validEmail}
                                        onChange={({target: {value}}) => this.setState({validEmail: isFieldValid(value)})}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="password">Password</Label>
                                    <Input
                                        type="password"
                                        name="password"
                                        id="password"
                                        placeholder="Password"
                                        invalid={!validPassword}
                                        onChange={({target: {value}}) => this.setState({validPassword: isFieldValid(value)})}
                                    />
                                </FormGroup>
                                <Button>Login</Button>
                            </Form>
                        </div>
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default Login
