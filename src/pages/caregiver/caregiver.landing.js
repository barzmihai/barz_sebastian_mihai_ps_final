import Home from "../../home/home";
import React from "react";
import {UserRole} from "../../commons/constants";

const CaregiverLanding = (props) => {
    return <Home role={UserRole.CAREGIVER}/>
}

export { CaregiverLanding };