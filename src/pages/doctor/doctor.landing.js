import Home from "../../home/home";
import React from "react";
import {UserRole} from "../../commons/constants";

const DoctorLanding = (props) => {
    return <Home role={UserRole.DOCTOR} />
}

export { DoctorLanding };