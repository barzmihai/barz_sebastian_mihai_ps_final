import {HOST} from '../../commons/hosts';
import {performRequest} from "../../commons/api/rest-client";


const endpoint = {
    doctors: '/doctors'
};

function getDoctors(callback) {
    let request = new Request(HOST.local_api + endpoint.doctors, {
        method: 'GET',
    });
    console.log(request.url);
    performRequest(request, callback);
}

function getDoctorsById(params, callback){
    let request = new Request(HOST.local_api + endpoint.doctors + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    performRequest(request, callback);
}

function postDoctor(user, callback){
    let request = new Request(HOST.local_api + endpoint.doctors , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    performRequest(request, callback);
}

function deleteDoctor(doctor, callback){
    let request = new Request(HOST.local_api + endpoint.doctors + "/" + doctor.name, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log("URL: " + request.url);

    performRequest(request, callback);
}

export {
    getDoctors,
    getDoctorsById,
    postDoctor,
    deleteDoctor
};
