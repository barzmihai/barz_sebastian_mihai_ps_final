import React from 'react'
import './styles.css';
import {Redirect, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import ErrorPage from './commons/errorhandling/error-page';
import Login from "./login/login";
import {ROLE_KEY, ROLES_WITH_PERMISSION} from "./commons/constants";
import {ProtectedRoute} from "./commons/protected.route";
import {isAuth} from "./commons/utils";
import {history} from "./index";
import {routes} from "./commons/routes";

class App extends React.Component {

    state = {
        hasMenuPermission: false,
        isLoggedIn: false,
    }

    componentDidMount() {
        const userRole = localStorage.getItem(ROLE_KEY);
        if (userRole !== undefined && userRole !== null) {
            this.setState({hasMenuPermission: ROLES_WITH_PERMISSION.includes(userRole), isLoggedIn: isAuth()});
        } else {
            this.setState({isLoggedIn: false});
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const userRole = localStorage.getItem(ROLE_KEY);
        let hasMenuPermission = false;
        let isLoggedIn = false;
        if (userRole !== undefined && userRole !== null) {
            hasMenuPermission = ROLES_WITH_PERMISSION.includes(userRole);
            isLoggedIn = true;
        }
        if (this.state.hasMenuPermission !== hasMenuPermission) {
            this.setState({hasMenuPermission});
        }
        if (this.state.isLoggedIn !== isLoggedIn) {
            this.setState({isLoggedIn});
        }
    }

    handleLogout = () => {
        localStorage.removeItem(ROLE_KEY);
        history.push('/login');
    }

    render() {
        const {hasMenuPermission, isLoggedIn} = this.state;
        return <div className={"h-100"}>
            <NavigationBar
                hasMenuPermission={hasMenuPermission}
                handleLogout={this.handleLogout}
                isLoggedIn={isLoggedIn}
                {...this.props}
            />
            <Switch>
                <Route
                    exact
                    path='/login'
                    render={props => isAuth() ? <Redirect to={'/'}/> : <Login {...props} />}
                />
                {routes.map(({component, ...rest}) =>
                    <ProtectedRoute key={rest.path} component={component} {...rest} />)
                }
                {/*Error*/}
                <Route
                    exact
                    path='/error'
                    component={ErrorPage}
                />
                <Route render={() => <ErrorPage/>}/>
            </Switch>
        </div>
    };
}

export default App
