import React from 'react'
import logo from './commons/images/icon.png';

import {
    Button,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = ({hasMenuPermission, handleLogout, isLoggedIn, ...props}) => {

    const logout = () => {
        handleLogout && handleLogout();
        // TODO redirect to '/login'
    }

    return <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} alt={"logo"}/>
            </NavbarBrand>
            <Nav className="mr-auto d-flex flex-grow-1 justify-content-between" navbar>
                {/* Left side of the navbar */}
                <section>
                    {hasMenuPermission && <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle style={textStyle} nav caret>
                            Menu
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink href="/person">Persons</NavLink>
                                <NavLink href="/doctors">Doctors</NavLink>
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                    }
                </section>
                {/* Right side of the navbar */}
                <section className={"d-flex flex-row align-items-center"}>
                    {isLoggedIn &&
                    <Button color={'primary'} size="sm" onClick={() => logout()} className={'justify-self-end'}>
                        Logout
                    </Button>
                    }
                </section>
            </Nav>
        </Navbar>
    </div>
};

export default NavigationBar
